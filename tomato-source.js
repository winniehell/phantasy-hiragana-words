const url = new URL(location.href);
const params = {
  words: 2,
  moras: 3,
  ...Object.fromEntries(url.searchParams)
};

Object.entries(params).forEach(([key, value]) => {
  const [inputElement] = document.getElementsByName(key);
  if (inputElement) {
    inputElement.value = value;
  }
});

const [form] = document.getElementsByTagName("form");
form.classList.remove("hidden");

const 清音 = `
  あ 	い 	う 	え 	お
  か 	き 	く 	け 	こ
  さ 	し 	す 	せ 	そ
  た 	ち 	つ 	て 	と
  な 	に 	ぬ 	ね 	の
  は 	ひ 	ふ 	へ 	ほ
  ま 	み 	む 	め 	も
  や 		 	ゆ 			よ
  ら 	り 	る 	れ 	ろ
  わ
  ん
`.trim().split(/\s+/);

const 濁点 = `
  が 	ぎ 	ぐ 	げ 	ご
  ざ 	じ 	ず 	ぜ 	ぞ
  だ 	ぢ 	  	で 	ど
  ば 	び 	ぶ 	べ 	ぼ
  ぱ 	ぴ 	ぷ 	ぺ 	ぽ
`.trim().split(/\s+/);

const permutations = (a, b) => a.map(x => b.map(y => x + y)).reduce((c, d) => c.concat(d));

const 拗音 = permutations(Array.from("きしちにひみりぎじびぴ"), Array.from("ゃゅょ"));

const 平仮名 = [
  ...清音,
  ...濁点,
  ...拗音
];

const result = document.getElementById("result");
for (let i = 0; i < params.words; i++) {
  let word = "";
  for (let j = 0; j < params.moras; j++) {
    word += 平仮名[Math.floor(Math.random() * 平仮名.length)];
  }
  result.innerText += word + " \n";
}
